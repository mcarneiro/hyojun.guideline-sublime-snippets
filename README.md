# Sublime text snippets para Hyojun.guideline

Projetinho para ajudar na criação dos elementos HTML para o projeto [Hyojun.guideline](http://fbiz.bitbucket.org/hyojun.guideline/modules.html). Exemplos de tags:

## Instalação

Clone este repositório dentro da pasta `packages` do seu Sublime Text. Ex.:

```
git clone git@bitbucket.org:mcarneiro/hyojun.guideline-sublime-snippets.git ~/Library/Application\ Support/Sublime\ Text\ 3/Packages/Hyojun.guideline 
```

## Como utilizar

Todos os snippets são ativados no "tab trigger". Apenas digite o nome e pressione tab para expandí-los.

A seleção do texto também é suportada:

```
<p class="gl">
	Adicione a classe .active para atualizar o estado do evento.
</p>
```

Selecione o texto ".active":

```
<p class="gl">
	Adicione a classe [.active] para atualizar o estado do evento.
</p>
```

Digite `command + shift + p`, escreva o nome do snippet `glcode` e pressione enter:

```
<p class="gl">
	Adicione a classe <code class="gl">.active</code> para atualizar o estado do evento.
</p>
```

### Lista de triggers disponíveis

Shortcut | output
--|--
`glheader` | `...<h2 class="gl-header"></h2>`
`glh` | `...<h2 class="gl"></h2>`
`glp` | `<p class="gl"></p>`
`glexample` | `<div class="gl-example-full gl-checkers-dark">`
`glpre` | `<pre class="gl"><code></code></pre>`
`glcode` | `<code class="gl"></code>`
`glnote` | `<strong class="gl-note-1"></strong>`
`glul` | `<ul class="gl">`
`glol` | `<ol class="gl">...`
`glcallout` | `<div class="gl-callout-important">...`
`glhr` | `<hr class="gl">`
`gllegend` | `<p class="gl-legend"></p>`
`glrow` | `<div class="gl-row">...`
`glhalf` | `<div class="gl-half">`
`glthird` | `<div class="gl-third">`